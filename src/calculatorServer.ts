import { Logger, ISettingsParam } from 'tslog';
import * as grpc from '@grpc/grpc-js';
import * as protoLoader from '@grpc/proto-loader';
import { ProtoGrpcType } from './proto/calculator';
import { CalculatorHandlers } from './proto/com/playground/generated/Calculator';
import { CalculatorRequest } from './proto/com/playground/generated/CalculatorRequest';
import { CalculatorResponse } from './proto/com/playground/generated/CalculatorResponse';

const host = '0.0.0.0:9090';
const devLogParams: ISettingsParam = {
    displayFilePath: 'hideNodeModulesOnly',
    displayRequestId: true,
    displayFunctionName: false,
    requestId: '',
    minLevel: 'debug',
};
const log: Logger = new Logger(devLogParams);

// temp global storage for convenience
let maxPairResult = -1;
let currentTotal = 0;

// to simulate delay
function sleep(ms: number) {
    return new Promise((resolve) => {
        setTimeout(resolve, ms)
    })
}

const calculatorServer: CalculatorHandlers = {
    sumOnce(
        call: grpc.ServerUnaryCall<CalculatorRequest, CalculatorResponse>,
        callback: grpc.sendUnaryData<CalculatorResponse>
    ) {
        log.info(`sumOnce: received (${call.request.number1}, ${call.request.number2})`)
        callback(null, {
            result: call.request.number1! + call.request.number2!,
        });
    },

    async deriveSubsequent(
        call: grpc.ServerWritableStream<CalculatorRequest, CalculatorResponse>
    ) {
        log.info(`deriveSubsequent: received (${call.request.number1}, ${call.request.number2})`);
        const diff = call.request.number2! - call.request.number1!
        let startingNum = call.request.number2!
        for (let i = 0; i < 10; ++i) {
            const result = startingNum + diff
            call.write({
                result: result
            })
            log.info(`deriveSubsequent: emitted next sequence ${result}`)
            await sleep(1000)
            startingNum = result
        }
    },

    getMaxPair(
        call: grpc.ServerReadableStream<CalculatorRequest, CalculatorResponse>,
        callback: grpc.sendUnaryData<CalculatorResponse>
    ) {
        call.on('data', (request: CalculatorRequest) => {
            log.info(`deriveSubsequent: received (${request.number1}, ${request.number2})`);
            const pairResult = request.number1! + request.number2!;
            maxPairResult = maxPairResult > pairResult ? maxPairResult : pairResult;
        });
        call.on('close', () => {
            log.info('deriveSubsequent: no more incoming data')
            callback(null, {
                result: maxPairResult,
            });
        })
    },

    getContinuousSum(
        call: grpc.ServerDuplexStream<CalculatorRequest, CalculatorResponse>
    ) {
        call.on('data', async (request: CalculatorRequest) => {
            log.info(`getContinuousSum: received (${request.number1}, ${request.number2})`);
            await sleep(1000)
            currentTotal += request.number1! + request.number2!
            call.write({
                result: currentTotal,
            });
            log.info(`getContinuousSum: emitted ${currentTotal}`);
        });
    },
};

function getServer(): grpc.Server {
    const packageDefinition = protoLoader.loadSync('./src/proto/calculator.proto');
    const proto = grpc.loadPackageDefinition(
        packageDefinition
    ) as unknown as ProtoGrpcType;
    const server = new grpc.Server();
    server.addService(proto.com.playground.generated.Calculator.service, calculatorServer);
    return server;
}

if (require.main === module) {
    const server = getServer();
    server.bindAsync(
        host,
        grpc.ServerCredentials.createInsecure(),
        (err: Error | null, port: number) => {
            if (err) {
                log.error(`Server error: ${err.message}`);
            } else {
                log.info(`Server bound on port: ${port}`);
                server.start();
            }
        }
    );
}
