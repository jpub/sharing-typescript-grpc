import { Logger, ISettingsParam } from 'tslog';
import * as grpc from '@grpc/grpc-js';
import * as protoLoader from '@grpc/proto-loader';
import { ProtoGrpcType } from './proto/calculator';
import { CalculatorResponse } from './proto/com/playground/generated/CalculatorResponse';

const host = '0.0.0.0:9090'; // node server
// const host = '0.0.0.0:50051'; // kotlin server
const devLogParams: ISettingsParam = {
    displayFilePath: 'hideNodeModulesOnly',
    displayRequestId: true,
    displayFunctionName: false,
    requestId: '',
    minLevel: 'debug',
};
const log: Logger = new Logger(devLogParams);

const packageDefinition = protoLoader.loadSync('./src/proto/calculator.proto');
const proto = grpc.loadPackageDefinition(
    packageDefinition
) as unknown as ProtoGrpcType;

const client = new proto.com.playground.generated.Calculator(
    host,
    grpc.credentials.createInsecure()
);

const deadline = new Date();
deadline.setSeconds(deadline.getSeconds() + 5);
client.waitForReady(deadline, (error?: Error) => {
    if (error) {
        log.info(`Client connect error: ${error.message}`);
    } else {
        onClientReady();
    }
});

// to simulate delay
function sleep(ms: number) {
    return new Promise((resolve) => {
        setTimeout(resolve, ms)
    })
}

function getRandom() {
    return Math.floor((Math.random() * 20) + 1);
}

function onClientReady() {
    switch (process.argv[process.argv.length - 1]) {
        case '--unary':
            doUnaryCall();
            break;
        case '--server-streaming':
            doServerStreamingCall();
            break;
        case '--client-streaming':
            doClientStreamingCall();
            break;
        case '--bidi-streaming':
            doBidirectionalStreamingCall();
            break;
        default:
            throw new Error('Example not specified');
    }
}

function doUnaryCall() {
    client.sumOnce(
        {
            number1: 1,
            number2: 2
        },
        (error?: grpc.ServiceError | null, response?: CalculatorResponse) => {
            if (error) {
                log.error(error.message);
            } else if (response) {
                log.info(`doUnaryCall: response: ${response.result}`);
            }
        }
    );
}

function doServerStreamingCall() {
    const stream = client.deriveSubsequent({
        number1: 1,
        number2: 5
    });
    stream.on('data', (response: CalculatorResponse) => {
        log.info(`doServerStreamingCall: response: ${response.result}`);
    });
}

async function doClientStreamingCall() {
    const stream = client.getMaxPair((error?: grpc.ServiceError | null, response?: CalculatorResponse) => {
        if (error) {
            log.error(error.message);
        } else if (response) {
            log.info(`doClientStreamingCall: response: ${response.result}`);
        }
    });
    for (let i = 0; i < 10; ++i) {
        const num1 = getRandom();
        const num2 = getRandom()
        stream.write({
            number1: num1,
            number2: num2
        });
        await sleep(1000);
        log.info(`doClientStreamingCall: emitted (${num1}, ${num2})`);
    }
    stream.end()
}

async function doBidirectionalStreamingCall() {
    const stream = client.getContinuousSum();

    // Server stream
    stream.on('data', (response: CalculatorResponse) => {
        log.info(`doBidirectionalStreamingCall: response ${response.result}`);
    });

    // Client stream
    for (let i = 0; i < 10; ++i) {
        const num1 = getRandom();
        const num2 = getRandom();
        stream.write({
            number1: num1,
            number2: num2
        });
        await sleep(3000);
        log.info(`doBidirectionalStreamingCall: emitted (${num1}, ${num2})`);
    }
}
